defmodule Kotlin do

  def classHt(name) do
    case name do
      HTML -> "html {\n    background-color: rgb(29, 29, 29);\n}\n"
      CODE -> "code {\n    font-family: Consolas, \"courier new\";\n    color:white;\n    padding: 2px;\n    font-size: 125%;\n}\n"
      KEYWORD-> "span.keyword {\n    color: rgb(106, 97, 165);\n}\n"
      DATAT -> "span.datat {\n    color: rgb(137, 200, 209);\n}\n"
      NUMBER -> "span.int, span.long, span.float {\n    color: rgb(140, 169, 248);\n}\n"
      COMMENT -> "span.comment, span.blockComment {\n    color: rgb(156, 191, 180);\n}\n"
      FUNCN -> "span.print, span.identifier {\n    color: rgb(255, 151, 71);\n}\n"
      PARAM -> "span.parametro {\n    color: rgb(246, 121, 130);\n}\n"
      STRING -> "span.string {\n    color: rgb(98, 138, 248);\n}\n"
      DELIM -> "span.delimiter {\n    color: rgb(241, 126, 168);\n}\n"
      OPER -> "span.operator {\n    color: rgb(167, 162, 139);\n}\n"
      FUN -> "span.function {\n    color: rgb(98, 138, 248);\n}\n"
    end
  end

  def spanC(name) do
    case name do
      :formato -> ""
      :keyword -> "<span class=\"keyword\">"
      :dataType -> "<span class=\"datat\">"
      :int -> "<span class=\"int\">"
      :long -> "<span class=\"long\">"
      :float -> "<span class=\"float\">"
      :comment -> "<span class=\"comment\">"
      :blockComment -> "<span class=\"blockComment\">"
      :print -> "<span class=\"print\">"
      :parametro -> "<span class=\"parametro\">"
      :constructor -> "<span class=\"identifier\">"
      :function -> "<span class=\"function\">"
      :identifierFun -> "<span class=\"identifier\">"
      :identifierClass -> "<span class=\"identifier\">"
      :identifierVal -> "<span class=\"keyword\">"
      :identifierVar -> "<span class=\"keyword\">"
      :delimiter -> "<span class=\"delimiter\">"
      :string -> "<span class=\"string\">"
      :row -> "<span class=\"string\">"
      :char -> "<span class=\"string\">"
      :operador -> "<span class=\"operator\">"
    end
  end

  def htmO(type) do

    case type do
      DOC -> "<!DOCTYPE html>\n"
      HTML -> "<html>\n"
      HEAD -> "<head>\n"
      H1 -> "<h1>"
      LINK -> "   <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\" media=\"screen\" /> \n"
      PRE -> "<pre>\n"
      CODE -> "<code>\n"
      SPAN -> "<span>"
      BODY -> "<body>\n"
      CLOSE -> ">"
    end

  end

  def htmC(type) do

    case type do
      HTML -> "</html>"
      HEAD -> "</head>\n"
      H1 -> "</h1>"
      PRE -> "</pre>\n"
      CODE -> "</code>\n"
      SPAN -> "</span>"
      BODY -> "</body>\n"
    end

  end

  def readF(fileName) do
    {:ok,file} = File.open("kotlinF/" <> fileName, [:read])
    IO.read(file, :all)
    |> String.to_charlist
    |> :lexer.string
  end

  def scan({:formato,_,code}) do
    string = to_string(code)
    string
  end

  def scan({:generic,_,code}) do
    string = to_string(code)
    string
  end

  def scan({:identifierVal,_,code}) do
    code = String.codepoints(to_string(code)) -- ["v","a","l"]
    string = spanC(:keyword) <> "val" <> htmC(SPAN) <>  to_string(code)
    string
  end

  def scan({:identifierVar,_,code}) do
    code = String.codepoints(to_string(code)) -- ["v","a","r"]
    string = spanC(:keyword) <> "var" <> htmC(SPAN) <>  to_string(code)
    string
  end

  def scan({:identifierFun,_,code}) do
    code = String.codepoints(to_string(code)) -- ["f","u","n"]
    string = spanC(:keyword) <> "fun" <> htmC(SPAN) <> spanC(:identifierFun) <> to_string(code) <> htmC(SPAN)
    string
  end

  def scan({:identifierClass,_,code}) do
    code = String.codepoints(to_string(code)) -- ["c","l","a","s","s"]
    string = spanC(:keyword) <> "class" <> htmC(SPAN) <> spanC(:identifierClass) <> to_string(code) <> htmC(SPAN)
    string
  end

  def scan({key,_, code}) do
    code = code |> to_string() |> HtmlEntities.encode()
    string = spanC(key) <> code <> htmC(SPAN)
    string
  end

  def tokensL({_,[],_},stringT), do: stringT

  def tokensL({_,[h|t],_},stringT) do
    string= stringT <> scan(h)
    tokensL({:ok,t,:ok},string)
  end

  def openF(filename \\ "prueba.kt") do
    code = readF(filename)
    {:ok, file} = File.open("htmlCssF/" <> String.replace_suffix(filename, ".kt",".html"), [:write])
    {:ok, file2} = File.open("htmlCssF/" <> String.replace_suffix(filename, ".kt",".css"), [:write])
    cssSt = String.replace_suffix(filename,".kt",".css")
    tab = "   "
    line = "\n"
    styles = "   <link rel=\"stylesheet\" type=\"text/css\" href=\"" <> cssSt <>"\" media=\"screen\" /> \n"
    openH = htmO(H1)
    closeH = htmC(H1)
    text = "Resaltador de codigo Kotlin"
    cadena = tab <> openH <> text <> closeH <> line
    cadena2 = [HTML, HEAD]|>Enum.map(fn n -> Kotlin.htmO(n) end)|>Enum.join
    estilos = [HTML, CODE, KEYWORD, DATAT, NUMBER, COMMENT, FUNCN, FUN, PARAM, STRING, DELIM, OPER]|>Enum.map(fn n -> Kotlin.classHt(n) end)|>Enum.join
    cadena4 = [HEAD]|>Enum.map(fn n -> Kotlin.htmC(n) end)|>Enum.join
    cadena5 = [BODY, PRE, CODE]|>Enum.map(fn n -> Kotlin.htmO(n) end)|>Enum.join
    cadena6 = [CODE, PRE, BODY]|>Enum.map(fn n -> Kotlin.htmC(n) end)|>Enum.join
    cadenaG = cadena2 <> styles <> cadena4 <> line <> cadena5 <> cadena <> line <> tokensL(code,"") <> line <> cadena6 <> line <> htmC(HTML) <> line
    IO.write(file2, estilos)
    IO.write(file, cadenaG)
    File.close(file)
  end

  def seql(folderName \\ "kotlinF") do
    {:ok, kotlinFiles} = File.ls(folderName)
    Enum.map(kotlinFiles, fn file -> openF(file) end)
  end

  def conc(folderName \\ "kotlinF") do
    {:ok, kotlinFiles} = File.ls(folderName)
    Enum.map(kotlinFiles, fn file -> Task.async(fn -> openF(file) end) end)
  end

  def benchmark(folderName \\ "kotlinF") do
    Benchee.run(
      %{
        "sequential" => fn -> seql(folderName) end,
        "concurrent" => fn -> conc(folderName) end
      }
    )
  end

end
