Definitions.

L = [A-Za-z]
M = [A-Z]
N = [0-9]
I = [0-9](\_[0-9]+)*
O = (\`|\_|:|@|\.(\.)?|\+(\+)?(\+)?|-(-)?(-)?|\!(=)?(=)?|\^(\^\^)?|~~~|\*|/|<>|\|>|<<<|>>>|<<~|~>>|<~|~>|<~>|<\|>|<|>|<=|>=|=(=)?(=)?|=~|&(&)?(&)?|\|(\|)?(\|)?|::|<-|\\|,|\%)

Rules.

%Spaces
\s|\n|\t|\r
: {token, {formato, TokenLine, TokenChars}}.

%Keywords

as|as\?|false|in|\!in|interface|is|\!is|null|object|package|super|this|true|typealias|typeof|import|out|override|private
: {token, {keyword, TokenLine, TokenChars}}.

%Data type

IntArray|String|Boolean|Int|Char|Byte|Short|Long|Any|Double|Float
: {token, {dataType, TokenLine, TokenChars}}.

%Numbers

{I}
: {token, {int, TokenLine, TokenChars}}.

{I}(l|L)?
: {token, {long, TokenLine, TokenChars}}.

{I}\.{I}(f|F)?
: {token, {float, TokenLine, TokenChars}}.

0x[0-9a-fA-F]+
: {token, {hexa, TokenLine, TokenChars}}.

%Comments

//(.|\s)*
: {token, {comment, TokenLine, TokenChars}}.

/\*([^*]|\*+[^/]|(/\*([^*]|\*+[^/])*\*/))*\*/
: {token, {blockComment, TokenLine, TokenChars}}.

%Prints

println|print|init
: {token, {print, TokenLine, TokenChars}}.

%List

({L}+|({L}+({N}*|\_*)*))\.(({L}+|({L}+({N}*|\_*)*)))
: {token, {formato, TokenLine, TokenChars}}.

%identifier

{M}+|({M}+({L}+({N}*|\_*)*{L}*(\?)*))
: {token, {dataType, TokenLine, TokenChars}}.

{L}+|({L}+({N}*|\_*)*)|{L}
: {token, {formato, TokenLine, TokenChars}}.

var\s({L}+|({L}+({N}*|\_*)*{L}*))
: {token, {identifierVar, TokenLine, TokenChars}}.

val\s({L}+|({L}+({N}*|\_*)*{L}*))
: {token, {identifierVal, TokenLine, TokenChars}}.

fun\s({L}+|({L}+({N}*|\_*)*{L}*))
: {token, {identifierFun, TokenLine, TokenChars}}.

class\s({L}+|({L}+({N}*|\_*)*{L}*))
: {token, {identifierClass, TokenLine, TokenChars}}.

%Parametro

{L}+[^\s\(\)]+\:
: {token, {parametro, TokenLine, TokenChars}}.

%Functions

for|while|if|when|return|break|continue|do|else|throw|try
: {token, {function, TokenLine, TokenChars}}.

%Delimiter

\(|\)|\[|\]|\{|\}
: {token, {delimiter, TokenLine, TokenChars}}.

%operadores

{O}|\?|;
: {token, {operador, TokenLine, TokenChars}}.

%Strings

"""([^\"]|\"|\n|\s|\t|\r)*"""
: {token, {row, TokenLine, TokenChars}}.

"([^\"\n]|\\")*"
: {token, {string, TokenLine, TokenChars}}.

[']([^\"\']|\\t|\\b|\\n|\\r|\\'|\"|\\uFF00)[']
: {token, {char, TokenLine, TokenChars}}.

(.)
: {token, {generic, TokenLine, TokenChars}}.

Erlang code.
